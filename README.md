# Git & GitLab for beginners

This repository contains the presentation and assignments for a course on Git and GitLab.

The slides for the course are contained in the `presentation` directory.
They are rendered using [marp-cli][1] in the CI: [pdf][2], [html][3].

## Presentation

The presentation covers the following topics:

- Git:
  - Why version control?
  - How Git is structured: what is a repository/commit/branch/merge?
  - Why Git, and not SVN/Mercurial/etc.?

- GitLab:
  - General UI: issues, merge requests
  - Code review
  - CI and Runners
  - Forking and contributing

## Assignments

### 1. Contributing

1. Fork this repository.
2. Create a new branch for your work.
3. Make a modification in this repository.
   This may be anything, but improvements will actually be merged.
4. Commit this change.
6. Create a *merge request* with your changes to this repository.

### 2. Resolve a merge conflict

1. Create a new branch in your fork of this repository (see assignment 1).
   The name of this branch should start with `fix-conflict`.
2. Merge the `conflict` branch into your branch.
3. Resolve the merge conflict.
4. Create a *merge request* with your changes.

[1]: https://github.com/marp-team/marp-cli
[2]: https://git.snt.utwente.nl/silke/git_course/-/jobs/57454/artifacts/browse
[3]: https://silke.utwente.io/git_course/
