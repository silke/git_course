---
theme: default
paginate: true
style: |
  img {
    display: block;
    margin: 0 auto;
  }
---

# Crash course on Git & GitLab

## For beginners

---

# Course overview

Two parts:

1. Lecture
2. Assignments

---

# Lecture overview

- Introduction
- **Git**
  - Version control
  - Approaches to version control
  - Git terminology
  - Git workflow
- GitLab
- Assignments

---

# Why version control?

Report progress:

- `Report.docx`
- `Report_19-4.docx`
- `FINAL.docx`
- `FINAL_rev2.docx`
- `FINAL_really.docx`
- `Report_FINAL.docx`

*Problem?*

---

# What is version control?

Store versions in an organised way:

![height:400px](img/scm_local.png)

<!-- source: git-scm.com -->

---

# Approaches to version control

Local version control:

![height:400px](img/scm_local.png)
<!-- source: git-scm.com -->

Example: Revision Control System (1982)

---

# Approaches to version control

Centralised version control:

![height:400px](img/scm_centralized.png)
<!-- source: git-scm.com -->

Example: Subversion (2000)

---

# Approaches to version control

Distributed version control:

![height:500px](img/scm_distributed.png)

Example: Git (2005), Mercurial (2005)

---

# Why distributed version control?

Best of both *local* and *centralised*:

- Everyone has local history
- You can *push* code to a server to share

Big difference:

- You can have multiple servers!

---

# Git terminology: the repo

- *Project*: the thing you are working on
- *Repository* or *repo*: folder/database containing all files/versions
- *Remote*: server hosting a repo of the same project
- *Cloning*: creating a local copy of a remote repository
- *Forking*: creating a remote copy of a remote repository

---

# Git terminology: branches, tags, refs

- Saved version is identified by a *reference* unique to the state of the project
  (e.g.: `82ff822eb6e8f2ac8cdfe4bb176e1e5cf1f21f62`)
- Create a reference with a *commit*
- Get to this reference using a *branch* or *tag*
- Branches and tags exist both *local* and *remote*
- Branches can *diverge* and *merge*

---

# Git terminology: overview

![height:600px](img/workflow.png)

<!-- source: https://www.reddit.com/r/git/comments/99ul9f/git_workflow_diagram_showcasing_the_role_of/
-->

---

# Git workflow: the goals

The goals of the *Git Workflow* are:

- Work together
- Work separately
- Work towards a goal
- Look at each other's work

This procedure is followed by small open-source projects and large enterprises alike.

---

# Git workflow: the big picture

![height:600px](img/workflow2.jpg)

---

# Git merges: handling conflicts

Example:

```markdown
If you have questions, please
<<<<<<< HEAD
open an issue.
=======
ask your question on Matrix.
>>>>>>> branch-a
```

Resolve by deleting the markers and the wrong content, e.g.:

```markdown
If you have questions, please
open an issue.
```

---

# Lecture overview

- Introduction

- Git

- **GitLab**
  - What does it do?
  - What can we use it for?
  - How can *we* help?

- Assignments

---

# What does GitLab do?

Host projects!

![height:500px](img/gitlab_explore.png)

---

# What can we use GitLab for? (1/3)

Project management:

- Track *issues*: features, bugs, tasks, etc...
- Merge *merge requests*: review code.
- Set *milestones* (goals).
- Track Git *branches* and *tags*

---

# What can we use GitLab for? (2/3)

Automation:

- Testing your project automatically.
- Building your project and hosting these automated builds.
- Deploying your project.

---

# What can we use GitLab for? (3/3)

Working with Git:

- GitLab puts most of Git in a GUI.
- Including an editor!

---

# Lecture overview

- Introduction
- Git
- GitLab
- **Assignments**

---

# Assignments

Two assignments:

1. Contributing
2. Resolving a merge conflict

Go to <https://git.snt.utwente.nl/silke/git_course>

![](img/repo_qr.png)

---

# Assignments: Git tips

1. Commit messages should be in active voice and present tense.
   For example: `Combine A and B to solve C.`
2. Git works best for text files.
   Avoid storing large binaries like `.docx` or `.pdf` as much as possible.
3. Add the names of files you don't want in your version control in `.gitignore`

---

# Git cheat sheet

Also see <https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet>

```sh
git clone https://git.snt.utwente.nl/silke/git_course.git # Clone this course

git checkout -b some-branch-name # Create a branch

git add <file> # Stage <file> for commit.

git commit # Commit all staged changes.

git push -u origin some-branch-name # Push and set the upstream of this branch
                                    # to `some-branch-name`

git push # Regular push

git merge other-branch-name # Merge another branch into this one
```
